package com.service;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class CalculatorTest {

	@BeforeClass
	public static void once() {
		//logic called only once during the test case
		System.out.println("called only once during the test case");
	}
	@Before
	public void init() {
		System.out.println("called every time @Test is valid");
	}
	@Test
	public void testAdd() {
		Calculator calculator = new Calculator();
		Assert.assertEquals(calculator.add(2, 3), 5);
	}
	
	@Test
	public void testAddTwo() {
		Calculator calculator = new Calculator();
		Assert.assertEquals(calculator.add(3, 4), 7);
	}
	
	@Test
	@Ignore
	public void testAddThree() {
		Calculator calculator = new Calculator();
		Assert.assertEquals(calculator.add(4, 4), 8);
	}
	
	@After
	public void destroy() {
		System.out.println("called every time @Test is completed");
	}
	
	@AfterClass
	public static void delete() {
		//logic called only once during the test case
		System.out.println("called only once during the test case close");
	}

}
