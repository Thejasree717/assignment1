package com.main;

import com.service.Calculator;

public class CalculatorMain {

	public static void main(String[] args) {
		
		Calculator calculator =new Calculator();
		int ans = calculator.add(2,3);
		System.out.println("Addition of 2 numbers : "+ans);
		
		int ans1 = calculator.add(3,3);
		System.out.println("Addition of 2 numbers : "+ans1);
	}
}
